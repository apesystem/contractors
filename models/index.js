var mongoose = require( 'mongoose' )
  , env, ape, iw, lv, usc, tt, uvm;

var mongo = {
  ape: 'mongodb://localhost:27017/ape?safe=true',
  lv: 'mongodb://localhost:27019/LV?safe=true'
};

ape = mongoose.createConnection(mongo.ape),
lv = mongoose.createConnection(mongo.lv);
console.log('connected to Mongo@ localhost:27017/ape', 'localhost: 27019/lv');

// General Models
module.exports.Athletes = ape.model('athletes', require('./schemas/athletes'));
module.exports.Coaches = ape.model('coaches', require('./schemas/coaches'));
module.exports.Schools = ape.model('schools', require('./schemas/schools'));
module.exports.Sessions = ape.model('sessions', require('./schemas/sessions'));

module.exports.models = function(school) {
  var models = {};

  switch(school) {
    case "University of Louisville":
      models.Metric = lv.model('metrics', require('./schemas/LV/metrics'));
      models.Biometric = lv.model('biometrics', require('./schemas/LV/bioMetrics'));
      models.Power = lv.model('powermetrics', require('./schemas/LV/power'));
      models.Speed = lv.model('speedmetrics', require('./schemas/LV/speed'));
      models.Strength = lv.model('strengthmetrics', require('./schemas/LV/strength'));
      models.RecordTime = lv.model('recordTimes', require('./schemas/LV/recordTime'));
      break;

    default:
      console.log('something went wrong: Models:index.js');
      break;
  }
  return models;
};
